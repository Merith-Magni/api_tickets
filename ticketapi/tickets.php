<?php
	// Connect to database
	include("db_connect.php");
	$request_method = $_SERVER["REQUEST_METHOD"];

	function getTickets()
	{
		global $conn;
		$query = "SELECT * FROM ticket";
		$response = array();
		$result = mysqli_query($conn, $query);
		while($row = mysqli_fetch_array($result))
		{
			$response[] = $row;
		}
		header('Content-Type: application/json');
		echo json_encode($response, JSON_PRETTY_PRINT);
	}
	
	function getTicket($id=0)
	{
		global $conn;
		$query = "SELECT * FROM ticket";
		if($id != 0)
		{
			$query .= " WHERE id=".$id." LIMIT 1";
		}
		$response = array();
		$result = mysqli_query($conn, $query);
		while($row = mysqli_fetch_array($result))
		{
			$response[] = $row;
		}
		header('Content-Type: application/json');
		echo json_encode($response, JSON_PRETTY_PRINT);
	}
	
	function AddTicket()
	{
		global $conn;
		$date = date('Y-m-d H:i:s');
		$probleme = $_POST["probleme"];
		$severite = $_POST["severite"];
		echo $query="INSERT INTO ticket(date, probleme, severite) VALUES('".$date."', '".$probleme."', '".$severite."')";
		if(mysqli_query($conn, $query))
		{
			$response=array(
				'status' => 1,
				'status_message' =>'ticket ajout� avec succ�s.'
			);
		}
		else
		{
			$response=array(
				'status' => 0,
				'status_message' =>'ERREUR!.'. mysqli_error($conn)
			);
		}
		header('Content-Type: application/json');
		echo json_encode($response);
	}
	
	function updateTicket($id)
	{
		global $conn;
		$_PUT = array();
		parse_str(file_get_contents('php://input'), $_PUT);
		$date = $_PUT["date"];
		$probleme = $_PUT["probleme"];
		$severite = $_PUT["severite"];
		$query="UPDATE ticket SET date='".$date."', probleme='".$probleme."', severite='".$severite."' WHERE id=".$id;
		
		if(mysqli_query($conn, $query))
		{
			$response=array(
				'status' => 1,
				'status_message' =>'Ticket mis a jour avec succes.'
			);
		}
		else
		{
			$response=array(
				'status' => 0,
				'status_message' =>'Echec de mise a jour du ticket. '. mysqli_error($conn)
			);
			
		}
		
		header('Content-Type: application/json');
		echo json_encode($response);
	}
	
	function deleteTicket($id)
	{
		global $conn;
		$query = "DELETE FROM ticket WHERE id=".$id;
		if(mysqli_query($conn, $query))
		{
			$response=array(
				'status' => 1,
				'status_message' =>'Ticket supprime avec succes.'
			);
		}
		else
		{
			$response=array(
				'status' => 0,
				'status_message' =>'La suppression du ticket a echoue. '. mysqli_error($conn)
			);
		}
		header('Content-Type: application/json');
		echo json_encode($response);
	}
	
	switch($request_method)
	{
		
		case 'GET':
			// Retrive Ticket
			if(!empty($_GET["id"]))
			{
				$id=intval($_GET["id"]);
				getTicket($id);
			}
			else
			{
				getTickets();
			}
			break;
		default:
			// Invalid Request Method
			header("HTTP/1.0 405 Method Not Allowed");
			break;
			
		case 'POST':
			// Ajouter un ticket
			AddTicket();
			break;
			
		case 'PUT':
			// Modifier un ticket
			$id = intval($_GET["id"]);
			updateTicket($id);
			break;
			
		case 'DELETE':
			// Supprimer un ticket
			$id = intval($_GET["id"]);
			deleteTicket($id);
			break;

	}
?>